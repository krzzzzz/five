//
// Created by Zoltan Krizsan on 2018. 11. 14..
//

#include <stdio.h>
#include <float.h>
#define CESTER_NO_MAIN
#include <exotic/cester.h>
#include <ufive/uFRI.h>
#include <ufive/uError.h>


#ifndef CALLOC_MOCK
#define CALLOC_MOCK
int * calloc_rv;
int FRI_calculateRuleDistanceFromObservation(struct FRI_RULE *pRule);

struct CALLOC_PARAMS {
    size_t nmemb;
    size_t size;
} callocParams;


#endif

CESTER_MOCK_FUNCTION(calloc(size_t nmemb, size_t size), void *,
                     callocParams.nmemb = nmemb;
                             callocParams.size = size;
                             return calloc_rv;
)

CESTER_TEST(FRI_linear_interpolation__NULL_param, inst,
    float result = 0.0;
    int errorCode = FRI_linear_interpolation(NULL, &result);
    cester_assert_equal(errorCode, WRONG_PARAMETER);
)

CESTER_TEST(FRI_linear_interpolation__happy_path, inst,
    // given
    
    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[2];
    termList[0].center = 1;
    termList[0].value = 1;
    termList[1].center = 3;
    termList[1].value = 3;
    
    universe.termList = termList;
    universe.sizeOfTerms = 2;
    universe.capacityOfTerms = 2;
    universe.observation = 2.0;
    float result = 0.0;
    
    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);
    
    //then
    cester_assert_equal(errorCode, 0);
    cester_assert_float_eq(result, 2.0);
)                                                                       
 

CESTER_TEST(FRI_linear_interpolation_4_rules, inst,
    // given

    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[4];
    termList[0].center = 1;
    termList[0].value = 1;
    termList[1].center = 3;
    termList[1].value = 3;
    termList[2].center = 5;
    termList[2].value = 5;
    termList[3].center = 7;
    termList[3].value = 7;

    universe.termList = termList;
    universe.sizeOfTerms = 4;
    universe.capacityOfTerms = 4;
    universe.observation = 5.0;
    float result = 0.0;

    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);

    //then
    cester_assert_equal(errorCode, 0);
    cester_assert_float_eq(result, 5.0);
)


CESTER_TEST(FRI_linear_interpolation_SameRules_ObsAfterLastX, inst,
    // given

    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[2];
    termList[0].center = 1;
    termList[0].value = 1;
    termList[1].center = 3;
    termList[1].value = 3;

    universe.termList = termList;
    universe.sizeOfTerms = 2;
    universe.capacityOfTerms = 2;
    universe.observation = 3.5;
    float result = 0.0;

    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);

    //then
    cester_assert_equal(errorCode, OBSERVATION_IS_NOT_COVERING);
)


CESTER_TEST(FRI_linear_interpolation_SameRules_ObsBeforeFirstX, inst,
    // given
    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[2];
    termList[0].center = 1;
    termList[0].value = 1;
    termList[1].center = 3;
    termList[1].value = 3;

    universe.termList = termList;
    universe.sizeOfTerms = 2;
    universe.capacityOfTerms = 2;
    universe.observation = 0.8;
    float result = 0.0;

    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);

    //then
    cester_assert_equal(errorCode, OBSERVATION_IS_NOT_COVERING);
)


CESTER_TEST(FRI_linear_interpolation_SameValues, inst,
    // given

    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[2];
    termList[0].center = 5;
    termList[0].value = 6;
    termList[1].center = 5;
    termList[1].value = 7;

    universe.termList = termList;
    universe.sizeOfTerms = 2;
    universe.capacityOfTerms = 2;
    universe.observation = 5.0;
    float result = 0.0;

    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);

    //then
    cester_assert_equal(errorCode, TWO_RULES_WITH_SAME_X);
)

CESTER_TEST(FRI_linear_interpolation_OneRuleOnly, inst,
    // given

    struct FRI_UNIVERSE universe;
    struct FRI_TERM termList[1];
    termList[0].center = 4;
    termList[0].value = 4;

    universe.termList = termList;
    universe.sizeOfTerms = 1;
    universe.capacityOfTerms = 1;
    universe.observation = 4.0;
    float result = 0.0;

    //when
    int errorCode = FRI_linear_interpolation(&universe, &result);

    //then
    cester_assert_equal(errorCode, ILLEGAL_UNIVERSE_SIZE);
)

CESTER_TEST(FRI_shepard__wrong_parameter, inst,
            cester_assert_equal(FRI_shepard(NULL), WRONG_PARAMETER);

            struct FRI_UNIVERSE consequent;
            struct FRI_RULE rules;
            struct FRI_RULEBASE friRuleBase;

                    friRuleBase.rules = NULL;
                    friRuleBase.consequent = &consequent;
            cester_assert_equal(FRI_shepard(&friRuleBase), WRONG_PARAMETER);

                    friRuleBase.rules = &rules;
                    friRuleBase.consequent = NULL;
            cester_assert_equal(FRI_shepard(&friRuleBase), WRONG_PARAMETER);
)

CESTER_TEST(FRI_shepard__happy_path, inst,

                    struct FRI_UNIVERSE consequent;
                    struct FRI_RULE rules;
                    struct FRI_RULEBASE friRuleBase;
// missing
                    friRuleBase.rules = &rules;
        friRuleBase.consequent = &consequent;
                    // cester_assert_equal(FRI_shepard(&friRuleBase), 0);
)

CESTER_TEST(FRI_initRuleBasesArray__wrong_parameter, inst,
// given
                int size = 0;
                struct FRI_RULEBASE *pRuleBases;
//when
                int errorCode = FRI_initRuleBasesArray(size, &pRuleBases);

//then
                cester_assert_equal(errorCode, WRONG_PARAMETER);
)



CESTER_TEST(FRI_initRuleBasesArray__memory_error, inst,
// given
        int size = 2;
        struct FRI_RULEBASE *pRuleBases;
        calloc_rv = NULL;
//when
        int errorCode = FRI_initRuleBasesArray(size, &pRuleBases);

//then
        cester_assert_equal(callocParams.nmemb, size);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_RULEBASE));

        cester_assert_equal(errorCode, RULE_BASES_FAILED_TO_GENERATE);
        cester_assert_null(pRuleBases);
        cester_assert_equal(g_numberOfUniverses, 0);
)

CESTER_TEST(FRI_initRuleBasesArray__happy_path , inst,
// given
        int size = 2;
        struct FRI_RULEBASE *pRuleBases;
        calloc_rv = &size;
//when
        int errorCode = FRI_initRuleBasesArray(size, &pRuleBases);
//then
        cester_assert_equal(callocParams.nmemb, size);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_RULEBASE));

        cester_assert_equal(errorCode, 0);
        cester_assert_not_null(pRuleBases);
)

CESTER_TEST(FRI_initRuleBaseByPtr__happy_path, inst,
// given
        struct FRI_RULEBASE ruleBase;
        struct FRI_UNIVERSE consequentUniverse;
        uint16_t capacityOfRules;

        calloc_rv = &capacityOfRules;
//when
        FRI_initRuleBaseByPtr(&ruleBase, capacityOfRules, &consequentUniverse);
//then
        cester_assert_equal(callocParams.nmemb, capacityOfRules);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_RULE));

        cester_assert_equal(ruleBase.capacityOfRules, capacityOfRules);
        cester_assert_equal(ruleBase.consequent, &consequentUniverse);
        cester_assert_equal(ruleBase.rules, calloc_rv);
)

CESTER_TEST(FRI_initRuleBaseByPtr__memory_error, inst,
// given
            struct FRI_RULEBASE ruleBase;
            struct FRI_UNIVERSE consequentUniverse;
            uint16_t capacityOfRules = 1;

            calloc_rv = NULL;
//when
        FRI_initRuleBaseByPtr(&ruleBase, capacityOfRules, &consequentUniverse);
//then
        cester_assert_equal(callocParams.nmemb, capacityOfRules);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_RULE));

        cester_assert_equal(ruleBase.capacityOfRules, capacityOfRules);
        cester_assert_equal(ruleBase.consequent, &consequentUniverse);
        cester_assert_equal(ruleBase.rules, calloc_rv);
)

CESTER_TEST(FRI_createUniverseArray__happy_path, inst,
// given
        struct FRI_UNIVERSE *pUniverses;
        uint16_t g_numberOfUniverses = 3;
        calloc_rv = &g_numberOfUniverses;
//when
        int rc = FRI_createUniverseArray(g_numberOfUniverses, &pUniverses);
//then
        cester_assert_equal(rc, 0);
        cester_assert_equal(callocParams.nmemb, g_numberOfUniverses);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_UNIVERSE));

        cester_assert_equal(pUniverses, calloc_rv);
)

CESTER_TEST(FRI_createUniverseArray__wrong_param, inst,
// given
        struct FRI_UNIVERSE *pUniverses;
        uint16_t g_numberOfUniverses = 0;
        calloc_rv = NULL;
//when
        int rc = FRI_createUniverseArray(g_numberOfUniverses, &pUniverses);
//then
        cester_assert_equal(rc, WRONG_PARAMETER);
)

CESTER_TEST(FRI_createUniverseArray__memory_error, inst,
// given
        struct FRI_UNIVERSE *pUniverses;
        int16_t g_numberOfUniverses = 1;
        calloc_rv = NULL;
//when
        int rc = FRI_createUniverseArray(g_numberOfUniverses, &pUniverses);
//then
        cester_assert_equal(rc, UNIVERSE_FAILED_TO_GENERATE);
)

CESTER_TEST(FRI_initUniverseByPtr__happy_path, inst,
// given
        struct FRI_UNIVERSE universe;
        int16_t g_numberOfUniverses = 1;
        calloc_rv = &g_numberOfUniverses;
//when
        int rc = FRI_initUniverseByPtr(&universe, g_numberOfUniverses, "name");
//then
        cester_assert_equal(rc, 0);
        cester_assert_equal(callocParams.nmemb, g_numberOfUniverses);
        cester_assert_equal(callocParams.size, sizeof(struct FRI_TERM));

        cester_assert_equal(universe.capacityOfTerms, g_numberOfUniverses);
        cester_assert_equal(universe.sizeOfTerms, 0);
        cester_assert_equal(universe.termList, calloc_rv);
)

CESTER_TEST(FRI_initUniverseByPtr__wrong_param, inst,
// given
                    int16_t g_numberOfUniverses = 0;
                    calloc_rv = &g_numberOfUniverses;
//when
        int rc = FRI_initUniverseByPtr(NULL, g_numberOfUniverses, "name");
//then
        cester_assert_equal(rc, WRONG_PARAMETER);
)

CESTER_TEST(FRI_initUniverseByPtr__memory_error, inst,
// given
            struct FRI_UNIVERSE universe;
                    int16_t g_numberOfUniverses = 1;
                    calloc_rv = NULL;
//when
        int rc = FRI_initUniverseByPtr(&universe, g_numberOfUniverses, "name");
//then
        cester_assert_equal(rc, UNIVERSE_FAILED_TO_GENERATE);
)

CESTER_TEST(FRI_initZ, inst,
// given

//when
        int rc = FRI_initZ();
//then
        cester_assert_equal(g_numberOfUniverses, 0);
        cester_assert_equal(g_numberOfRuleBases, 0);
)

CESTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__NULL, inst,
// given
//when
        int rc = FRI_calculateRuleDistanceFromObservation(NULL);
//then
        cester_assert_equal(rc, WRONG_PARAMETER);
)

CESTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__EMPTY_RULE, inst,
// given
        struct FRI_RULE rule;
        rule.antecedents = NULL;
        rule.numberOfAntecedents = 0;
        rule.capacityOfAntecedents = 0;
//when
        int rc = FRI_calculateRuleDistanceFromObservation(&rule);
//then
        cester_assert_equal(rc, 0);
)

CESTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__ONE_ANTECEDENT, inst,
// given
        struct FRI_UNIVERSE speedUniverse;
        static struct FRI_TERM speedUniverseTermList[2];
        static struct FRI_TERM *pLowSpeedTerm;
        pLowSpeedTerm = &speedUniverseTermList[0];
        pLowSpeedTerm->center = 0;
        pLowSpeedTerm->value = 0;
        pLowSpeedTerm->name = "low";
        static struct FRI_TERM *pHighSpeedTerm;
        pHighSpeedTerm = &speedUniverseTermList[1];
        pHighSpeedTerm->center = 1000;
        pHighSpeedTerm->value = 1;
        pHighSpeedTerm->name = "high";

        speedUniverse.termList = speedUniverseTermList;
        speedUniverse.name = "speed";
        speedUniverse.capacityOfTerms = 2;
        speedUniverse.sizeOfTerms = 2;
        speedUniverse.observationValue = 600;

        struct FRI_RULE closeDistanceLowSpeedRule;
        closeDistanceLowSpeedRule.capacityOfAntecedents = 1;
        closeDistanceLowSpeedRule.numberOfAntecedents = 1;
        closeDistanceLowSpeedRule.termIdForConsequent = 0;
        static struct ANTECEDENT antecedentList[1];
        struct ANTECEDENT *pFirstAntecedent = &antecedentList[0];
        pFirstAntecedent->predicate = &speedUniverse;
        pFirstAntecedent->termIdForAntecedent = 0;
        closeDistanceLowSpeedRule.antecedents = antecedentList;
        closeDistanceLowSpeedRule.description = "IF distance IS close THEN speed IS low";
//when
        int rc = FRI_calculateRuleDistanceFromObservation(&closeDistanceLowSpeedRule);
//then
        cester_assert_equal(rc, 0);
        cester_assert_float_eq(closeDistanceLowSpeedRule.distance, 600.0);
)

CESTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__TWO_ANTECEDENT, inst,
// given

        struct FRI_TERM speedUniverseTermList[2];
        struct FRI_TERM *pLowSpeedTerm;
        pLowSpeedTerm = &speedUniverseTermList[0];
        pLowSpeedTerm->center = 0;
        pLowSpeedTerm->value = 0;
        pLowSpeedTerm->name = "low";
        struct FRI_TERM *pHighSpeedTerm;
        pHighSpeedTerm = &speedUniverseTermList[1];
        pHighSpeedTerm->center = 1000;
        pHighSpeedTerm->value = 1;
        pHighSpeedTerm->name = "high";

        struct FRI_UNIVERSE speedUniverse;
        speedUniverse.termList = speedUniverseTermList;
        speedUniverse.name = "speed";
        speedUniverse.capacityOfTerms = 2;
        speedUniverse.sizeOfTerms = 2;
        speedUniverse.observationValue = 600;

        struct FRI_UNIVERSE speedUniverse2;
        speedUniverse2.termList = speedUniverseTermList;
        speedUniverse2.name = "speed2";
        speedUniverse2.capacityOfTerms = 2;
        speedUniverse2.sizeOfTerms = 2;
        speedUniverse2.observationValue = 100;

        struct FRI_RULE closeDistanceLowSpeedRule;
        closeDistanceLowSpeedRule.capacityOfAntecedents = 2;
        closeDistanceLowSpeedRule.numberOfAntecedents = 2;
        closeDistanceLowSpeedRule.termIdForConsequent = 0;
        struct ANTECEDENT antecedentList[2];
        struct ANTECEDENT *pFirstAntecedent = &antecedentList[0];
        pFirstAntecedent->predicate = &speedUniverse;
        pFirstAntecedent->termIdForAntecedent = 0;
        struct ANTECEDENT *pSecondAntecedent = &antecedentList[1];
        pSecondAntecedent->predicate = &speedUniverse2;
        pSecondAntecedent->termIdForAntecedent = 0;

        closeDistanceLowSpeedRule.antecedents = antecedentList;
        closeDistanceLowSpeedRule.description = "IF distance IS close THEN speed IS low";
//when
        int rc = FRI_calculateRuleDistanceFromObservation(&closeDistanceLowSpeedRule);
//then
        cester_assert_equal(rc, 0);
        cester_assert_true(fabs(closeDistanceLowSpeedRule.distance - 430.116272) <= FLT_EPSILON);
)

CESTER_TEST(FRI_setObservationForUniverse__NULL_OBSERVATION, inst,
// given
        struct FRI_UNIVERSE universe;
        float observation = 0.5f;
//when
        int rc = FRI_setObservationForUniverse(NULL, observation);
//then
        cester_assert_equal(rc, WRONG_PARAMETER);
)

CESTER_TEST(FRI_termToString__NULL, inst,
// given

//when
        const char *string = FRI_termToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_termToString__NOT_NULL, inst,
// given
        struct FRI_TERM term;
        term.center = 12.0f;
        term.value = 1.0f;
        term.name = "name";
//when
            const char *string = FRI_termToString(NULL);
//then
        cester_assert_not_null(string);
)

CESTER_BODY(
        int main(int argc, char** argv) {
            CESTER_REGISTER_TEST(FRI_linear_interpolation__NULL_param);
            CESTER_REGISTER_TEST(FRI_linear_interpolation__happy_path);
            CESTER_REGISTER_TEST(FRI_linear_interpolation_4_rules);
            CESTER_REGISTER_TEST(FRI_linear_interpolation_SameRules_ObsAfterLastX);
            CESTER_REGISTER_TEST(FRI_linear_interpolation_SameRules_ObsBeforeFirstX);
            CESTER_REGISTER_TEST(FRI_linear_interpolation_SameValues);
            CESTER_REGISTER_TEST(FRI_linear_interpolation_OneRuleOnly);
            CESTER_REGISTER_TEST(FRI_shepard__wrong_parameter);
            CESTER_REGISTER_TEST(FRI_shepard__happy_path);
            CESTER_REGISTER_TEST(FRI_initRuleBasesArray__wrong_parameter);
            CESTER_REGISTER_TEST(FRI_initRuleBasesArray__happy_path);
            CESTER_REGISTER_TEST(FRI_initRuleBasesArray__memory_error);
            CESTER_REGISTER_TEST(FRI_initRuleBaseByPtr__happy_path);
            CESTER_REGISTER_TEST(FRI_initRuleBaseByPtr__memory_error);
            CESTER_REGISTER_TEST(FRI_createUniverseArray__happy_path);
            CESTER_REGISTER_TEST(FRI_createUniverseArray__wrong_param);
            CESTER_REGISTER_TEST(FRI_createUniverseArray__memory_error);
            CESTER_REGISTER_TEST(FRI_initUniverseByPtr__happy_path);
            CESTER_REGISTER_TEST(FRI_initUniverseByPtr__wrong_param);
            CESTER_REGISTER_TEST(FRI_initUniverseByPtr__memory_error);
            CESTER_REGISTER_TEST(FRI_initZ);
            CESTER_REGISTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__NULL);
            CESTER_REGISTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__EMPTY_RULE);
            CESTER_REGISTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__ONE_ANTECEDENT);
            CESTER_REGISTER_TEST(FRI_calculateAntecedentsDistanceFromObservation__TWO_ANTECEDENT);
            CESTER_REGISTER_TEST(FRI_setObservationForUniverse__NULL_OBSERVATION);
            CESTER_REGISTER_TEST(FRI_termToString__NULL);
            CESTER_REGISTER_TEST(FRI_termToString__NOT_NULL);

            return CESTER_RUN_ALL_TESTS(argc, argv);
        }
)
