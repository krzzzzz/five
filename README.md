# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### visual studio setup ###
1. clone the https://github.com/exoticlibraries/libcester.git repository
2. open the five project as directory
3. click right on CMakeList.txt in root folder
4. click on CMake Settings for five
5. click Edit JSON (top right corner)
6. add new environment to CMakeSettings.json
 
 
 ```
 {
          "name": "CESTER_HOME",
          "value": "PATH_TO_CESTER_CLONE",
          "type": "FILEPATH"
 }
  ```
### compile from command line under linux
1. mkdir build
2. cd build
3. 
```
cmake -DCMAKE_BUILD_TYPE=Debug -DCESTER_HOME:FILEPATH=/home/kry/devel/c/libcester ..
```
4. cmake --build .
5. cmake --build . --target install

### compile from clion under linux
1. add CESTER_HOME to cmake CACHE, -DCESTER_HOME:FILEPATH=CLONED_CESTER_HOME_FULL_PATH

### Suggested IDE: clion
pls modify the CMake settings
update the CMake options of Debug profile (and the others)
-DCMAKE_BUILD_TYPE=Debug -DCESTER_HOME:FILEPATH=/home/kry/devel/c/libcester
https://www.jetbrains.com/help/clion/cmake-profile.html#compiler-flags

