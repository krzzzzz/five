/*
 * main.c
 *
 *  Created on: 2016.02.25.
 *      Author: Roland
 */
#include "stdio.h"
#include "include/ufive/uFRI.h" // new thing

int main(int argc, char **argv) {

	float m_observation = 0;
	float resultHW = 0;
	float resultSW = 0;
	float epsilon = 0.1f;

	float orientation_error = -100;
	float orientation = 0;
	uint8_t i, numberOfErrors = 0;
	uint8_t state;
	FRI_init(4, 2);

	// es ha tobb univerzumot akar inicializalni, mint ami van?
	FRI_initUniverseById(0,3); //Universe ID: 0
	FRI_addUniverseElement(0, 0);
	FRI_addUniverseElement(5, 2);
	FRI_addUniverseElement(10, 10);

	FRI_initUniverseById(1,3); //Universe ID: 1
	FRI_addUniverseElement(0, 0);
	FRI_addUniverseElement(5, 2);
	FRI_addUniverseElement(10, 10);

	FRI_initUniverseById(2,2); //Universe ID: 2
	FRI_addUniverseElement(0, 0);
	FRI_addUniverseElement(10, 10);

	FRI_initUniverseById(3, 2); //Universe ID: 3
	FRI_addUniverseElement(0, 0);
	FRI_addUniverseElement(10, 10);

	FRI_initRuleBaseById(0, 2, 2); //The consequent Universe ID: 2
	FRI_addRuleToRulebase(0, 2);
	FRI_addAntecedentToRule(0, 0);
	FRI_addAntecedentToRule(1, 0);

	FRI_addRuleToRulebase(1, 2);
	FRI_addAntecedentToRule(0, 2);
	FRI_addAntecedentToRule(1, 1);

	FRI_initRuleBaseById(1, 2, 3); //The consequent Universe ID: 3
	FRI_addRuleToRulebase(0, 2);
	FRI_addAntecedentToRule(0, 0);
	FRI_addAntecedentToRule(1, 0);

	FRI_addRuleToRulebase(1, 2);
	FRI_addAntecedentToRule(0, 2);
	FRI_addAntecedentToRule(1, 1);

	for (i = 0; i <= 20; i++) {
		FRI_setObservationForUniverseById(0, m_observation);
		FRI_setObservationForUniverseById(1, m_observation);
		m_observation += 0.5;

		FRI_calculateAllRuleBases();
		
		printf("**Rulebase: %lf\t%lf\n\n", FRI_getObservationById(2), FRI_getObservationById(3));
	}
	
	printf("\nExited\n");
	//char c;
	//scanf("%c", &c);
	FRI_clean();
	return 0;
}

/*
struct UNIVERSE test_universe;
struct UNIVERSE test_universe_2;
struct UNIVERSE test_universe_3;
struct RULEBASE test_rulebase;
struct RULE test_rule[2];

struct ANTECEDENT test_antecedent_1;
struct ANTECEDENT test_antecedent_2;
struct ANTECEDENT test_antecedent_3;
struct ANTECEDENT test_antecedent_4;

test_universe.capacity = 3;
test_universe.size = 3;
test_universe.uData = calloc(3,
sizeof(struct UNIVERSE_DATA));
test_universe.uData[0].x = 0;
test_universe.uData[0].y = 0;
test_universe.uData[1].x = 5;
test_universe.uData[1].y = 2;
test_universe.uData[2].x = 10;
test_universe.uData[2].y = 10;
test_universe.observation = 0;

test_universe_3.capacity = 3;
test_universe_3.size = 3;
test_universe_3.uData = calloc(3,
sizeof(struct UNIVERSE_DATA));
test_universe_3.uData[0].x = 0;
test_universe_3.uData[0].y = 0;
test_universe_3.uData[1].x = 5;
test_universe_3.uData[1].y = 2;
test_universe_3.uData[2].x = 10;
test_universe_3.uData[2].y = 10;
test_universe_3.observation = 0;


test_universe_2.capacity = 2;
test_universe_2.size = 2;
test_universe_2.uData = calloc(2,
sizeof(struct UNIVERSE_DATA));
test_universe_2.uData[0].x = 0;
test_universe_2.uData[0].y = 0;
test_universe_2.uData[1].x = 10;
test_universe_2.uData[1].y = 10;
test_universe_2.observation = 0;

test_antecedent_1.predicate = &test_universe;
test_antecedent_1.universeDataForAntecedent = 0;

test_antecedent_3.predicate = &test_universe_3;
test_antecedent_3.universeDataForAntecedent = 0;

test_antecedent_4.predicate = &test_universe_3;
test_antecedent_4.universeDataForAntecedent = 1;

test_antecedent_2.predicate = &test_universe;
test_antecedent_2.universeDataForAntecedent = 2;

test_rule[0].numberOfAntecedents = 2;
test_rule[0].universeDataForConsequent = 0;
test_rule[0].antecedents = malloc(2*sizeof(struct ANTECEDENT));
test_rule[0].antecedents[0] = test_antecedent_1;
test_rule[0].antecedents[1] = test_antecedent_3;

test_rule[1].numberOfAntecedents = 2;
test_rule[1].universeDataForConsequent = 1;
test_rule[1].antecedents = malloc(2*sizeof(struct ANTECEDENT));
test_rule[1].antecedents[0] = test_antecedent_2;
test_rule[1].antecedents[1] = test_antecedent_4;

test_rulebase.numberOfRules = 2;
test_rulebase.consequent = &test_universe_2;
test_rulebase.rules = test_rule;



orientation = 0;
orientation_error = 0;
for (i = 0; i < 20; i++) {
test_universe.observation += 0.5;
test_universe.observationValue = linear_interpolation(&test_universe);
test_universe_3.observation += 0.5;
test_universe_3.observationValue = linear_interpolation(&test_universe_3);
printf("%lf: %lf\n", test_universe.observation, test_universe.observationValue);
calculateDistanceForRule(&test_rule[0]);
printf("Rule1 d: %lf\n", test_rule[0].distance);
calculateDistanceForRule(&test_rule[1]);
printf("Rule2 d: %lf\n", test_rule[1].distance);
shepard(&test_rulebase);
printf("**Rulebase: %lf\n\n", test_universe_2.observation);
}
cleanFRI();
*/