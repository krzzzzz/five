/*
 * main.c
 *
 *  Created on: 2016.02.25.
 *      Author: Roland
 */
#include <ufive/uError.h>
#include "stdio.h"
#include "../include/ufive/uFRI.h" // new thing

int main() {

    float observation = 0;
    uint8_t i;
    if (FRI_init(4, 2) != OK) {
        exit(-1);
    }

    // es ha tobb univerzumot akar inicializalni, mint ami van?
    FRI_initUniverseById(0,3, "1. universe"); //Universe ID: 0
    FRI_addUniverseElement(0, 0, "term1");
    FRI_addUniverseElement(5, 2, "term2");
    FRI_addUniverseElement(10, 10, "term3");

    FRI_initUniverseById(1,3, "2. universe"); //Universe ID: 1
    FRI_addUniverseElement(0, 0, "term1");
    FRI_addUniverseElement(5, 2, "term2");
    FRI_addUniverseElement(10, 10, "term3");

    // consequents

    #define CONSEQUENT_1UNIVERSE_ID 2
    FRI_initUniverseById(CONSEQUENT_1UNIVERSE_ID,2, "3. universe"); //Universe ID: 2
    FRI_addUniverseElement(0, 0, "term1");
    FRI_addUniverseElement(10, 10, "term2");


    #define CONSEQUENT_2UNIVERSE_ID 3
    FRI_initUniverseById(CONSEQUENT_2UNIVERSE_ID, 2, "3. universe"); //Universe ID: 3
    FRI_addUniverseElement(0, 0, "term1");
    FRI_addUniverseElement(10, 10, "term2");

    // rule bases

    FRI_initRuleBaseById(0, 2, CONSEQUENT_1UNIVERSE_ID); //The consequent Universe ID: 2
    FRI_addRuleToRuleBase(0, 2);
    FRI_addAntecedentToRule(0, 0);
    FRI_addAntecedentToRule(1, 0);

    FRI_addRuleToRuleBase(1, 2);
    FRI_addAntecedentToRule(0, 2);
    FRI_addAntecedentToRule(1, 1);

    FRI_initRuleBaseById(1, 2, CONSEQUENT_2UNIVERSE_ID); //The consequent Universe ID: 3
    FRI_addRuleToRuleBase(0, 2);
    FRI_addAntecedentToRule(0, 0);
    FRI_addAntecedentToRule(1, 0);

    FRI_addRuleToRuleBase(1, 2);
    FRI_addAntecedentToRule(0, 2);
    FRI_addAntecedentToRule(1, 1);

    printf("%s\n", FRI_ruleToString(& g_ruleBases[0] ));
    for (i = 0; i <= 20; i++) {
        FRI_setObservationForUniverseById(0, observation);
        FRI_setObservationForUniverseById(1, observation);
        observation += 0.5;

        FRI_calculateAllRuleBases();

        printf("**Rulebase: %lf\t%lf\n\n", FRI_getObservationById(2), FRI_getObservationById(3));
    }

    printf("\nExited\n");
    FRI_clean();

    return 0;
}