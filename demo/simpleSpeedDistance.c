/*
 * simpleSpeedDistance.c
 * C implementation of http://mat76.mat.uni-miskolc.hu/ethology/simulation/base
 *  Created on: 2020 10 30
 *      Author: KrZ
 *
 *

universe "distance"
    "close" 0 0
    "far" 720 1
end

universe "speed"
    "low" 0 0
    "high" 1 1
end

rulebase "speed"
    rule
        "low" when "distance" is "close"
    end
    rule
        "high" when "distance" is "far"
    end
end
 *
 *
 *
 */
#include "stdio.h"
#include "../include/ufive/uFRI.h"

float scanDistance() {
    float inputDistance;
    scanf("%f", &inputDistance);

    if (inputDistance < 0) {
        printf("distance must be 0 or positive");
        exit(-1);
    }
    return inputDistance;
}

int initFuzzy() {
    if (!FRI_init(2, 1)) {
        exit(-1);
    }
#define DISTANCE_UNIVERSE 0
#define SPEED_UNIVERSE 1
#define SPEED_RULE_BASE 0
#define CLOSE_DISTANCE 0
#define FAR_DISTANCE 1
#define LOW_SPEED 0
#define HIGH_SPEED 1
    FRI_initUniverseById(DISTANCE_UNIVERSE, 2, "distance"); //Universe ID: 0
    FRI_addUniverseElement(0, CLOSE_DISTANCE, "close");
    FRI_addUniverseElement(720, FAR_DISTANCE, "far");

    FRI_initUniverseById(SPEED_UNIVERSE, 2, "speed"); //Universe ID: 1
    FRI_addUniverseElement(0, LOW_SPEED, "low");
    FRI_addUniverseElement(1, HIGH_SPEED, "high");

    FRI_initRuleBaseById(SPEED_RULE_BASE, 2, 1); //The consequent Universe ID: 2

    FRI_addRuleToRuleBase(LOW_SPEED, 1);
    FRI_addAntecedentToRule(DISTANCE_UNIVERSE, CLOSE_DISTANCE);

    FRI_addRuleToRuleBase(HIGH_SPEED, 1);
    FRI_addAntecedentToRule(DISTANCE_UNIVERSE, FAR_DISTANCE);
}

int initFuzzyByPointers() {

    static struct FRI_UNIVERSE universeList[2];
    static struct FRI_TERM distanceTermList[2];
    struct FRI_UNIVERSE *pDistanceUniverse = &universeList[0];
    struct FRI_UNIVERSE *pSpeedUniverse = &universeList[1];
    struct FRI_TERM *pCloseDistance = &distanceTermList[0];
    struct FRI_TERM *pFarDistance = &distanceTermList[1];
    pCloseDistance->center = 0;
    pCloseDistance->value = 0;
    pCloseDistance->name = "close";
    pFarDistance->center = 720;
    pFarDistance->value = 1;
    pFarDistance->name = "far";

    pDistanceUniverse->termList = distanceTermList;
    pDistanceUniverse->name = "distance";
    pDistanceUniverse->capacityOfTerms = 2;
    pDistanceUniverse->sizeOfTerms = 2;

    static struct FRI_TERM speedUniverseTermList[2];
    static struct FRI_TERM *pLowSpeedTerm;
    pLowSpeedTerm = &speedUniverseTermList[0];
    pLowSpeedTerm->center = 0;
    pLowSpeedTerm->value = 0;
    pLowSpeedTerm->name = "low";
    static struct FRI_TERM *pHighSpeedTerm;
    pHighSpeedTerm = &speedUniverseTermList[1];
    pHighSpeedTerm->center = 1;
    pHighSpeedTerm->value = 1;
    pHighSpeedTerm->name = "high";

    pSpeedUniverse->termList = speedUniverseTermList;
    pSpeedUniverse->name = "speed";
    pSpeedUniverse->capacityOfTerms = 2;
    pSpeedUniverse->sizeOfTerms = 2;

    static struct FRI_RULE ruleList[2];
    struct FRI_RULE *pCloseDistanceLowSpeedRule;
    pCloseDistanceLowSpeedRule = &ruleList[0];
    pCloseDistanceLowSpeedRule->capacityOfAntecedents = 1;
    pCloseDistanceLowSpeedRule->numberOfAntecedents = 1;
    pCloseDistanceLowSpeedRule->termIdForConsequent = 0;
    static struct ANTECEDENT antecedent1;
    antecedent1.predicate = pDistanceUniverse;
    antecedent1.termIdForAntecedent = 0;
    pCloseDistanceLowSpeedRule->antecedents = &antecedent1;
    pCloseDistanceLowSpeedRule->description = "IF distance IS close THEN speed IS low";

    struct FRI_RULE *pFarDistanceHighSpeedRule;
    pFarDistanceHighSpeedRule = &ruleList[1];
    pFarDistanceHighSpeedRule->capacityOfAntecedents = 1;
    pFarDistanceHighSpeedRule->numberOfAntecedents = 1;
    pFarDistanceHighSpeedRule->termIdForConsequent = 1;
    static struct ANTECEDENT antecedent2;
    antecedent2.predicate = pDistanceUniverse;
    antecedent2.termIdForAntecedent = 1;
    pFarDistanceHighSpeedRule->antecedents = &antecedent2;
    pFarDistanceHighSpeedRule->description = "IF distance IS far THEN speed IS high";

    static struct FRI_RULEBASE ruleBaseList[1];
    struct FRI_RULEBASE *pSpeedRuleBase;
    pSpeedRuleBase = &ruleBaseList[0];
    pSpeedRuleBase->capacityOfRules = 2;
    pSpeedRuleBase->numberOfRules = 2;
    pSpeedRuleBase->consequent = pSpeedUniverse;
    pSpeedRuleBase->rules = ruleList;

    g_universes = universeList;
    g_ruleBases = ruleBaseList;
    g_numberOfUniverses = 2;
    g_numberOfRuleBases = 1;
    g_lastInitializedRuleBase = 0;
    g_lastInitializedUniverse = 1;
    g_lastInitializedRule = 2;
}

void printResponse(float input) {
    FRI_setObservationForUniverseById(0, input);
    FRI_calculateAllRuleBases();

    printf("result speed: %f", FRI_getObservationById(1));
}

int main(int argc, char **argv) {
    float inputDistance;
    if (argc > 1) {
        inputDistance = atof(argv[1]);
    } else {
        inputDistance = scanDistance();
    }

    initFuzzyByPointers();
    // initFuzzy();
    char *res;
    printResponse(inputDistance);
    free(res);
    res = FRI_toString();
    printf("\nafter \n%s", res);
    free(res);
    //   FRI_clean();
    return 0;
}