//
// Created by Zoltan Krizsan on 2018. 11. 18..
//

#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include "../include/ufive/uFRI.h"
#define UNIT_TESTING 1
// #include <cmocka.h>
#include <stdio.h>

int setup() {

    int actUniverseSize = 0;
    //universes
    // observation
    //  0. cold
    //  1. warm

    // conclusion air conditioner functioning
    //  2. on, off

    //rules
    // if cold then on
    // if warm then off

    // we need 3 universes & 2 rule bases
    assert_int_equal(FRI_init(3, 2), 0);

    // cold
    assert_int_equal(FRI_initUniverseSizeById(0,2), 0);
    assert_int_equal(FRI_addUniverseDataById(0, 10, 1, &actUniverseSize), 0); // 10 celsius is cold
    assert_int_equal(actUniverseSize, 1);
    assert_int_equal(FRI_addUniverseDataById(0, 22, 0, &actUniverseSize), 0); // 23 celsius is not cold
    assert_int_equal(actUniverseSize, 2);

    // warm
    assert_int_equal(FRI_initUniverseSizeById(1,2), 0);
    assert_int_equal(FRI_addUniverseDataById(1, 10, 0, &actUniverseSize), 0); // 10 celsius is not warm
    assert_int_equal(actUniverseSize, 1);
    assert_int_equal(FRI_addUniverseDataById(1, 22, 1, &actUniverseSize), 0); // 23 celsius is warm
    assert_int_equal(actUniverseSize, 2);

    // switch
    assert_int_equal(FRI_initUniverseSizeById(2,2), 0);
    assert_int_equal(FRI_addUniverseDataById(2, 0, 0, &actUniverseSize), 0); // switch off the machine
    assert_int_equal(actUniverseSize, 1);
    assert_int_equal(FRI_addUniverseDataById(2, 1, 1, &actUniverseSize), 0); // switch off the machine
    assert_int_equal(actUniverseSize, 2);

    assert_int_equal(FRI_initRuleBaseById(0, 1, 2), 0); // 0. rule, 1 rule inside, id fo universe
    // value
    assert_int_equal(FRI_addRuleToRulebase(2, 2), 0); //  0. consequent 2 antecedents
    // if
    assert_int_equal(FRI_addAntecedentToRule(0, 1), 0); // 0. universe the antecedent 1. index element
    // and
    assert_int_equal(FRI_addAntecedentToRule(1, 0), 1); // 0. universe the antecedent 0. index element


    assert_int_equal(FRI_initRuleBaseById(1, 1, 2), 0); // 1. rule
    // value
    assert_int_equal(FRI_addRuleToRulebase(2, 2), 0); // 1. consequent 2 antecedents
    // if
    assert_int_equal(FRI_addAntecedentToRule(0, 0), 0); // 0. universe the antecedent 0. index element
    // and
    assert_int_equal(FRI_addAntecedentToRule(1, 1), 1);// 0. universe the antecedent 1. index element

    return 0;
}

int tear_down() {
    FRI_clean();
    return 0;
}


void test_10_cesius() {
    FRI_setObservationForUniverseById(0, 10);
    FRI_setObservationForUniverseById(1, 10);
    FRI_calculateAllRuleBases();
    assert_int_equal(FRI_getObservationById(2), 0);
}

void test_22_cesius() {
    FRI_setObservationForUniverseById(0, 22);
    FRI_setObservationForUniverseById(1, 22);
    FRI_calculateAllRuleBases();
    assert_int_equal(FRI_getObservationById(2), 1);
}

int main(void)
{
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(test_10_cesius),
            cmocka_unit_test(test_22_cesius)
    };

    return cmocka_run_group_tests(tests, setup, tear_down);

}