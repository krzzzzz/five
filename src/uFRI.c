/*
 * uFRI.c
 *
 *  Created on: 2017.04.08.
 *    
 */

#include <ufive/uFRI.h>
#include <ufive/uError.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

struct FRI_UNIVERSE *g_universes = NULL;
struct FRI_RULEBASE *g_ruleBases = NULL;

const float dimension_sqrt[] = { 1.0f, 1.0f, 1.414213562f, 1.732050808f, 2.0f, 2.236067977f,
		2.449489743f, 2.645751311f, 2.828427125f, 3.0f, 3.16227766f, 3.31662479f,
		3.464101615f, 3.605551275f, 3.741657387f, 3.872983346f, 4.0f, 4.123105626f,
		4.242640687f, 4.358898944f, 4.472135955f, 4.582575695f, 4.69041576f,
		4.795831523f, 4.898979486f, 5.0f, 5.099019514f, 5.196152423f, 5.291502622f,
		5.385164807f, 5.477225575f, 5.567764363f, 5.656854249f, 5.744562647f,
		5.830951895f, 5.916079783f, 6.0f, 6.08276253f, 6.164414003f, 6.244997998f,
		6.32455532f, 6.403124237f, 6.480740698f, 6.557438524f, 6.633249581f,
		6.708203932f, 6.782329983f, 6.8556546f, 6.92820323f, 7.0f, 7.071067812f,
		7.141428429f, 7.211102551f, 7.280109889f, 7.348469228f, 7.416198487f,
		7.483314774f, 7.549834435f, 7.615773106f, 7.681145748f, 7.745966692f,
		7.810249676f, 7.874007874f, 7.937253933f, 8.0f, 8.062257748f, 8.124038405f,
		8.185352772f, 8.246211251f, 8.306623863f, 8.366600265f, 8.426149773f,
		8.485281374f, 8.544003745f, 8.602325267f, 8.660254038f, 8.717797887f,
		8.774964387f, 8.831760866f, 8.888194417f, 8.94427191f, 9.0f, 9.055385138f,
		9.110433579f, 9.16515139f, 9.219544457f, 9.273618495f, 9.327379053f,
		9.38083152f, 9.433981132f, 9.486832981f, 9.539392014f, 9.591663047f,
		9.643650761f, 9.695359715f, 9.746794345f, 9.797958971f, 9.848857802f,
		9.899494937f, 9.949874371f, 10.0f, 10.04987562f, 10.09950494f, 10.14889157f,
		10.19803903f, 10.24695077f, 10.29563014f, 10.34408043f, 10.39230485f,
		10.44030651f, 10.48808848f, 10.53565375f, 10.58300524f, 10.63014581f,
		10.67707825f, 10.72380529f, 10.77032961f, 10.81665383f, 10.86278049f,
		10.90871211f, 10.95445115f, 11.0f, 11.04536102f, 11.09053651f, 11.13552873f,
		11.18033989f, 11.22497216f, 11.26942767f, 11.3137085f, 11.35781669f,
		11.40175425f, 11.44552314f, 11.48912529f, 11.53256259f, 11.5758369f,
		11.61895004f, 11.66190379f, 11.70469991f, 11.74734012f, 11.78982612f,
		11.83215957f, 11.87434209f, 11.91637529f, 11.95826074f, 12.0f, 12.04159458f,
		12.08304597f, 12.12435565f, 12.16552506f, 12.20655562f, 12.24744871f,
		12.28820573f, 12.32882801f, 12.36931688f, 12.40967365f, 12.4498996f,
		12.489996f, 12.52996409f, 12.56980509f, 12.60952021f, 12.64911064f,
		12.68857754f, 12.72792206f, 12.76714533f, 12.80624847f, 12.84523258f,
		12.88409873f, 12.92284798f, 12.9614814f, 13.0f, 13.03840481f, 13.07669683f,
		13.11487705f, 13.15294644f, 13.19090596f, 13.22875656f, 13.26649916f,
		13.3041347f, 13.34166406f, 13.37908816f, 13.41640786f, 13.45362405f,
		13.49073756f, 13.52774926f, 13.56465997f, 13.60147051f, 13.6381817f,
		13.67479433f, 13.7113092f, 13.74772708f, 13.78404875f, 13.82027496f,
		13.85640646f, 13.89244399f, 13.92838828f, 13.96424004f, 14.0f, 14.03566885f,
		14.07124728f, 14.10673598f, 14.14213562f, 14.17744688f, 14.2126704f,
		14.24780685f, 14.28285686f, 14.31782106f, 14.35270009f, 14.38749457f,
		14.4222051f, 14.45683229f, 14.49137675f, 14.52583905f, 14.56021978f,
		14.59451952f, 14.62873884f, 14.6628783f, 14.69693846f, 14.73091986f,
		14.76482306f, 14.79864859f, 14.83239697f, 14.86606875f, 14.89966443f,
		14.93318452f, 14.96662955f, 15.0f, 15.03329638f, 15.06651917f, 15.09966887f,
		15.13274595f, 15.16575089f, 15.19868415f, 15.23154621f, 15.26433752f,
		15.29705854f, 15.32970972f, 15.3622915f, 15.39480432f, 15.42724862f,
		15.45962483f, 15.49193338f, 15.5241747f, 15.55634919f, 15.58845727f,
		15.62049935f, 15.65247584f, 15.68438714f, 15.71623365f, 15.74801575f,
		15.77973384f, 15.8113883f, 15.84297952f, 15.87450787f, 15.90597372f,
		15.93737745f, 15.96871942f
};

uint16_t g_numberOfUniverses = 0;
uint16_t g_numberOfRuleBases = 0;
uint16_t g_lastInitializedUniverse = 0;
uint16_t g_lastInitializedRule = 0;
uint16_t g_lastInitializedRuleBase = 0;

int isCovering(float observation, const struct FRI_TERM *actualUniverse, const struct FRI_TERM *nextUniverse) {
    assert( actualUniverse != NULL && "actualUniverse must be non-NULL");
    assert( nextUniverse != NULL && "nextUniverse must be non-NULL");

    return observation >= actualUniverse->center && observation <= nextUniverse->center;
}

/**
 * @brief Interpolates a result (y) in the observation (universe->observation). Please note, it can not extrapolate.
 * 
 * @param universe pointer to the universe (main data structure)
 * @param result The response value;
 * @return int error code, 0 if success
 * 
 */
int FRI_linear_interpolation(const struct FRI_UNIVERSE const *pUniverse, float *pResult)
{																		 
	if (pUniverse == NULL)
	{		
		return WRONG_PARAMETER; 
	}

	if (pUniverse->sizeOfTerms == 1) return ILLEGAL_UNIVERSE_SIZE;

	uint16_t i;
	uint16_t selectedIndex = 0;
	int isObservationNOTCovered = 1;

	for (i = 0; i < pUniverse->sizeOfTerms - 1; ++i) {
		if (isCovering(pUniverse->observation, &pUniverse->termList[i], &pUniverse->termList[i + 1])) {
			selectedIndex = i;
			isObservationNOTCovered = 0;
			break;
		}
	}
		
	if (isObservationNOTCovered) return  OBSERVATION_IS_NOT_COVERING;
	if (pUniverse->termList[selectedIndex].center == pUniverse->termList[selectedIndex + 1].center)	 return TWO_RULES_WITH_SAME_X;

	float m = (pUniverse->termList[selectedIndex + 1].value - pUniverse->termList[selectedIndex].value)
		/ (pUniverse->termList[selectedIndex + 1].center - pUniverse->termList[selectedIndex].center);

	float b = -m * pUniverse->termList[selectedIndex].center + pUniverse->termList[selectedIndex].value;

	*pResult = m * pUniverse->observation + b;
	return 0;
}


int FRI_setObservationForUniverse(struct FRI_UNIVERSE *universe, float observation) {
    if (universe == NULL) {
        return WRONG_PARAMETER;
    }

	universe->observation = observation;
	float result;
	int errorCode = FRI_linear_interpolation(universe, &result);
	if (errorCode != 0) {
		fprintf(stderr, "\n %d - in this function this error has occurred.\n", errorCode);
		return errorCode;
	}
	
	universe->observationValue = result;

	return errorCode;
}

/// update the distance of the pRule
int FRI_calculateRuleDistanceFromObservation(struct FRI_RULE *pRule) {
    if (pRule == NULL) {
        return WRONG_PARAMETER;
    }
	uint16_t i;
	float distance = 0;
	for (i = 0; i < pRule->numberOfAntecedents; ++i) {
	    const float observationValue = pRule->antecedents[i].predicate->observationValue;
	    const int antecedentUniverseId = pRule->antecedents[i].termIdForAntecedent;
        pRule->antecedents[i].distance = (float)fabs((double)(pRule->antecedents[i].predicate->termList[antecedentUniverseId].value - observationValue));
		distance += pRule->antecedents[i].distance * pRule->antecedents[i].distance;
	}
	if (pRule->numberOfAntecedents < 255) {
        pRule->distance = sqrtf(distance) / dimension_sqrt[pRule->numberOfAntecedents];
	}
	else
	{
        pRule->distance = sqrtf(distance) / sqrtf(pRule->numberOfAntecedents);
	}

	return 0;
}

int calculateConclusion(const struct FRI_RULEBASE const *pRruleBase, float *ruleBaseConclusion) {
    uint16_t nExactHits = 0;
    float ruleConsequentValue;
    for (int i = 0; i < pRruleBase->numberOfRules; i++)
    {
        ruleConsequentValue = pRruleBase->consequent->termList[pRruleBase->rules[i].termIdForConsequent].value;
        if (pRruleBase->rules[i].distance == 0) {
            nExactHits++;
            *ruleBaseConclusion += ruleConsequentValue;
        }
    }

    return nExactHits;
}

int FRI_shepard(struct FRI_RULEBASE *pRuleBase) {
    if ((pRuleBase == NULL) || (pRuleBase->consequent == NULL) || (pRuleBase->rules == NULL)){
        return WRONG_PARAMETER;
    }

    float ruleConsequentValue;
	uint16_t i = 0;
	float sumOfWeights = 0;
	float weight = 0;
    float *ruleBaseConclusion = &pRuleBase->consequent->observation;
	*ruleBaseConclusion = 0;

    uint16_t nExactHits = calculateConclusion(pRuleBase, ruleBaseConclusion);

    if (nExactHits > 0) {
        *ruleBaseConclusion /= (float)nExactHits;
	} else {
		//Shepard interpolation
		for (i = 0; i < pRuleBase->numberOfRules; ++i) {
            ruleConsequentValue = pRuleBase->consequent->termList[pRuleBase->rules[i].termIdForConsequent].value;
			weight = 1.0f / (pRuleBase->rules[i].distance);
			sumOfWeights += weight;
            *ruleBaseConclusion += weight * ruleConsequentValue;
		}
        *ruleBaseConclusion /= sumOfWeights;
	}
	return 0;
}

void FRI_calculateAllRuleBases() {
	uint16_t i, j;

	for (i = 0; i < g_numberOfRuleBases; ++i) {
		for (j = 0; j < g_ruleBases[i].numberOfRules; ++j) {
            FRI_calculateRuleDistanceFromObservation(&g_ruleBases[i].rules[j]);
		}
		FRI_shepard(&g_ruleBases[i]);
	}
}


