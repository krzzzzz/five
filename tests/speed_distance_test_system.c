//
// Created by Kry on 11/5/2020.
//

/*
universe "distance"
"close" 0 0
"far" 720 1
end
        universe "speed"
"low" 0 0
"high" 1 1
end
        rulebase "speed"
rule
"low" when "distance" is "close"
end
        rule
"high" when "distance" is "far"
end
        end
*
*
*
*/

#include <ufive/uFRI.h>


int initFuzzyByPointers() {

    static struct FRI_UNIVERSE universeList[2];
    static struct FRI_TERM distanceTermList[2];
    struct FRI_UNIVERSE *pDistanceUniverse = &universeList[0];
    struct FRI_UNIVERSE *pSpeedUniverse = &universeList[1];
    struct FRI_TERM *pCloseDistance = &distanceTermList[0];
    struct FRI_TERM *pFarDistance = &distanceTermList[1];
    pCloseDistance->center = 0;
    pCloseDistance->value = 0;
    pCloseDistance->name = "close";
    pFarDistance->center = 720;
    pFarDistance->value = 1;
    pFarDistance->name = "far";

    pDistanceUniverse->termList = distanceTermList;
    pDistanceUniverse->name = "distance";
    pDistanceUniverse->capacityOfTerms = 2;
    pDistanceUniverse->sizeOfTerms = 2;
    pDistanceUniverse->observation = 0.0f;
    pDistanceUniverse->observationValue = 0.0f;

    static struct FRI_TERM speedUniverseTermList[2];
    static struct FRI_TERM *pLowSpeedTerm;
    pLowSpeedTerm = &speedUniverseTermList[0];
    pLowSpeedTerm->center = 0;
    pLowSpeedTerm->value = 0;
    pLowSpeedTerm->name = "low";
    static struct FRI_TERM *pHighSpeedTerm;
    pHighSpeedTerm = &speedUniverseTermList[1];
    pHighSpeedTerm->center = 1;
    pHighSpeedTerm->value = 1;
    pHighSpeedTerm->name = "high";

    pSpeedUniverse->termList = speedUniverseTermList;
    pSpeedUniverse->name = "speed";
    pSpeedUniverse->capacityOfTerms = 2;
    pSpeedUniverse->sizeOfTerms = 2;
    pSpeedUniverse->observation = 0.0f;
    pSpeedUniverse->observationValue = 0.0f;

    static struct FRI_RULE ruleList[2];
    struct FRI_RULE *pCloseDistanceLowSpeedRule;
    pCloseDistanceLowSpeedRule = &ruleList[0];
    pCloseDistanceLowSpeedRule->capacityOfAntecedents = 1;
    pCloseDistanceLowSpeedRule->numberOfAntecedents = 1;
    pCloseDistanceLowSpeedRule->termIdForConsequent = 0;
    static struct ANTECEDENT antecedent1;
    antecedent1.predicate = pDistanceUniverse;
    antecedent1.termIdForAntecedent = 0;
    pCloseDistanceLowSpeedRule->antecedents = &antecedent1;
    pCloseDistanceLowSpeedRule->description = "IF distance IS close THEN speed IS low";
    pCloseDistanceLowSpeedRule->distance = 0.0f;

    struct FRI_RULE *pFarDistanceHighSpeedRule;
    pFarDistanceHighSpeedRule = &ruleList[1];
    pFarDistanceHighSpeedRule->capacityOfAntecedents = 1;
    pFarDistanceHighSpeedRule->numberOfAntecedents = 1;
    pFarDistanceHighSpeedRule->termIdForConsequent = 1;
    static struct ANTECEDENT antecedent2;
    antecedent2.predicate = pDistanceUniverse;
    antecedent2.termIdForAntecedent = 1;
    pFarDistanceHighSpeedRule->antecedents = &antecedent2;
    pFarDistanceHighSpeedRule->description = "IF distance IS far THEN speed IS high";
    pFarDistanceHighSpeedRule->distance = 0.0f;

    static struct FRI_RULEBASE ruleBaseList[1];
    struct FRI_RULEBASE *pSpeedRuleBase;
    pSpeedRuleBase = &ruleBaseList[0];
    pSpeedRuleBase->capacityOfRules = 2;
    pSpeedRuleBase->numberOfRules = 2;
    pSpeedRuleBase->consequent = pSpeedUniverse;
    pSpeedRuleBase->rules = ruleList;

    g_universes = universeList;
    g_ruleBases = ruleBaseList;
    g_numberOfUniverses = 2;
    g_numberOfRuleBases = 1;
    g_lastInitializedRuleBase = 0;
    g_lastInitializedUniverse = 1;
    g_lastInitializedRule = 2;
}

void clearGlobals() {
    if (g_universes != NULL) {

    }
    g_universes = NULL;
    g_ruleBases = NULL;
    g_numberOfUniverses = 0;
    g_numberOfRuleBases = 0;
    g_lastInitializedRuleBase = 0;
    g_lastInitializedUniverse = 0;
    g_lastInitializedRule = 0;
}