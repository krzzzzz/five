/*
 * uFRI.h
 *
 *  Created on: 2017.04.08.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef UFRI_H_
#define UFRI_H_

#include "stdint.h"
#include "stdlib.h"
#include "math.h"

/// maximum number of character in name of any item
#define MAX_ITEM_NAME_LENGTH 100
/// maximum number of characters in the description.
#define MAX_DESCRIPTION_LENGTH 1000
/// maximum number of characters in toString() functions.
#define MAX_TEMP_STRING_LENGTH 100000

extern struct FRI_UNIVERSE *g_universes;
extern struct FRI_RULEBASE *g_ruleBases;
extern uint16_t g_numberOfUniverses;
extern uint16_t g_numberOfRuleBases;
extern uint16_t g_lastInitializedUniverse;
extern uint16_t g_lastInitializedRule;
extern uint16_t g_lastInitializedRuleBase;

/** The base element for g_universes is the UNIVERSE_DATA structure, which stores the x and y coordinates of universe.*/
struct FRI_TERM { // custom universe data, 1 1 2 2  uj fri universe data ervenyes adatokkal
    /// center of term (aka x coordinate)
	float center;
    /// value of term (aka y coordinate)
	float value;
    /// symbolic name of term
	char *name;
};

/**
 * The UNIVERSE_DATA elements are stored in the UNIVERSE structure as array.
 * The data elements must be arranged in ascending order by x value in the UNIVERSE.
 * The other elements of UNIVERSE are sizeOfTerms and capacityOfTerms, which are for dynamic memory handling.
 * The observation is also stored in the UNIVERSE. This data comes from the input sensors linked to the UNIVERSE.*/
struct FRI_UNIVERSE {
    /// current number of terms in the universe.
	uint16_t sizeOfTerms;
	/// maximum number of terms in the universe.
	uint16_t capacityOfTerms;
	/// terms objects.
	struct FRI_TERM *termList;
	float observation;
	float observationValue;
	/// symbolic name of universe
	char *name;
};

struct
ANTECEDENT {
	struct FRI_UNIVERSE *predicate;
	uint16_t termIdForAntecedent;
	float distance;
};

/** The RULE structure has got 4 scalar elements where it stores its own capacityOfTerms and the number of antecedents
 * and stores the order number of the UNIVERSE_DATA elements in the UNIVERSE, which is the consequent.
 * Also stores the rule distance.*/
struct FRI_RULE {
	uint16_t termIdForConsequent;
	uint16_t numberOfAntecedents;
	uint16_t capacityOfAntecedents;
	struct ANTECEDENT *antecedents;
	float distance;
	char * description;
};

/** The RULEBASE structure stores the following: its own capacityOfTerms, pointer for UNIVERSE structure
 * (which stands for consequent) an array for the RULES structure.*/
struct FRI_RULEBASE {
	uint16_t numberOfRules;
	uint16_t capacityOfRules;
	struct FRI_UNIVERSE *consequent;
	struct FRI_RULE *rules;
};

/**
 * Set the antecedent part of the pRule by pointer.
 @param pRule Pointer to pRule
 @param pPredicate Pointer to Universe which is the pPredicate
 @param universeDataForAntecedent Id (index) of Universe data which is the antecedent
 */
int FRI_addAntecedentToRuleByPtr(
	struct FRI_RULE *pRule,
	struct FRI_UNIVERSE *pPredicate,
	uint16_t universeDataForAntecedent);

/**
 * set the antecedents for the rule function.
@param idOfRuleBase identifier of rule base
@param idOfRule the identifier of rule
@param antecedentUniverse the identifier of antecedent universe
@param p_universeDataForAntecedent the identifier of UNIVERSE_DATA element in antecedent universe.
*/
int FRI_addAntecedentToRuleById(
	uint16_t idOfRuleBase,
	uint16_t idOfRule,
	uint16_t antecedentUniverse,
	uint16_t p_universeDataForAntecedent);

int FRI_addUniverseDataByPtr(struct FRI_UNIVERSE *pDestination, float x, float y, char *name);

/**
 * The data elements can be added with this function.
@param id identifier of universe
@param x data
@param y data
 @param name name of universe
*/
int FRI_addUniverseDataById(uint16_t id, float x, float y, const char const *name);

/**
Compute consequents for all RULEBASEs in the system.
*/
void FRI_calculateAllRuleBases();

/**
Compute the desired rulebase only.
@param idOfRuleBase ID of desired Rulebase
*/
void FRI_calculateRuleBaseById(uint16_t idOfRuleBase);

/**
Clear all data (g_ruleBases and g_universes) from the knowledge base.
*/
void FRI_clean();

/**
 * To create universes first one must call this function.
    @param p_numberOfUniverses Number of Universes to create.
    @param pUniverses result pointer.
*/
int FRI_createUniverseArray(uint16_t p_numberOfUniverses, struct FRI_UNIVERSE **pUniverses);

/**
Get the observation which set. Or get the conclusion.
@param idOfUniverse The ID of desired Universe.
*/
float FRI_getObservationById(uint16_t idOfUniverse);

struct FRI_UNIVERSE *FRI_getUniverse(uint16_t idOfUniverse);

/**
Initialization with all zeroes.
*/
int FRI_initZ();

/**
Initializes storage.
@param numberOfUniverses Number of g_universes used by rules (UINT16MAX)
@param numberOfRuleBases Number of g_ruleBases (UINT16MAX)
*/
int FRI_init(uint16_t numberOfUniverses, uint16_t numberOfRuleBases);

/**
The knowledge base is stored in g_ruleBases. The first step to create a RULEBASE is done with call this function
@param p_numberOfRuleBases the number of g_ruleBases to create.
@param p_ruleBases result pointer
*/
int FRI_initRuleBasesArray(uint16_t p_numberOfRuleBases, struct FRI_RULEBASE **p_ruleBases);

int FRI_initRuleBaseByPtr(
	struct FRI_RULEBASE *pRuleBase,
	uint16_t numberOfRules,
	struct FRI_UNIVERSE *pConsequentUniverse);

/**
Initialization of a RuleBase
@param: the identifier number of UNIVERSE
@param: the number of elements of UNIVERSE_DATA
*/
void FRI_initRuleBaseById(uint16_t idOfRuleBase, uint16_t numberOfRules,
	uint16_t idOfConsequentUniverse);

int FRI_initRuleByPtr(
	struct FRI_RULE *pRule,
	uint16_t numberOfAntecedents,
	uint16_t consequentData);

/**
Initialize a rule. 
@param: the identifier of the ruleBase
@param: the identifier of the rule
@param: the number of antecedents (observations)
@param: the consequent UNIVERSE_DATA identifier from consequent UNIVERSE.
*/
void FRI_initRuleById(
	uint16_t idOfRuleBase,
	uint16_t idOfRule,
	uint16_t numberOfAntecedents,
	uint16_t consequentData);

int FRI_initUniverseByPtr(struct FRI_UNIVERSE *universe, uint16_t numberOfUniverseData, const char const *name);

/**
Init a Universe
@param: ID of Universe
@param: Number of data elements.
*/
int FRI_initUniverseById(uint16_t id, uint16_t numberOfUniverseData, const char const *name);

/**
Sets the input data in the universe.
@param: Desired Universe
@param: The data.
*/
int FRI_setObservationForUniverse(struct FRI_UNIVERSE *universe, float observation);

void FRI_setObservationForUniverseById(uint16_t id, float observation);

/** Add new Universe data element to the last initialized Universe.
Call after FRI_initUniverseById(id, numberOfElements);
The elements must be in ascending order!
@param: x coordinate
@param y coordinate */
int FRI_addUniverseElement(float x, float y, const char const *name);

/**
Add new Rule to last initialized rulebase.
Call after FRI_initRuleBaseById()
@param: Id of element from Universe for consequent
@param: number of antecedents
*/
void FRI_addRuleToRuleBase(uint16_t consequentUniverseData, uint16_t numberOfAntecedents);

/**
Add new Antecedent to last initialized Rule.
Call after FRI_initRuleById() OR FRI_addRuleToRuleBase()
@param: ID of Universe for Antecedent
@param ID of element from Universe
*/
void FRI_addAntecedentToRule(uint16_t antecedentUniverse, uint16_t elementOfUniverseForAntecedent);

int FRI_linear_interpolation(const struct FRI_UNIVERSE const *pUniverse, float* result);
int FRI_shepard(struct FRI_RULEBASE *pRuleBase);
char *FRI_toString();
char *FRI_ruleBaseToString(const struct FRI_RULEBASE *pRuleBase);
char *FRI_ruleToString(const struct FRI_RULE *pRule);
char *FRI_universeToString(const struct FRI_UNIVERSE const *pUniverse);
char *FRI_termToString(const struct FRI_TERM const *pTerm);
char *FRI_antecedentToString(const struct ANTECEDENT const *pAntecedent);

#endif /* UFRI_H_ */
#ifdef __cplusplus
}
#endif