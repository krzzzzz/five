//
// Created by Zoltan Krizsan on 2018. 11. 14..
//

#include <stdio.h>
#include <float.h>

#define CESTER_NO_MAIN
#include <exotic/cester.h>
#include <ufive/uFRI.h>
#include <ufive/uError.h>


CESTER_TEST(FRI_termToString__NULL, inst,
// given

//when
        const char *string = FRI_termToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_termToString__NOT_NULL, inst,
// given
        struct FRI_TERM term;
        term.center = 12.0f;
        term.value = 1.0f;
        term.name = "name";
//when
        const char *string = FRI_termToString(&term);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "\"name\", center: 12.000000, value: 1.000000");
        free(string);
)

CESTER_TEST(FRI_termToString__NAME_NULL, inst,
// given
            struct FRI_TERM term;
                    term.center = 12.0f;
                    term.value = 1.0f;
                    term.name = NULL;
//when
        const char *string = FRI_termToString(&term);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "\"\", center: 12.000000, value: 1.000000");
        free(string);
)

CESTER_TEST(FRI_universeToString__NULL, inst,
// given

//when
        const char *string = FRI_universeToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_universeToString__NOT_NULL, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
            struct FRI_UNIVERSE universe;
            universe.name = "universe";
            universe.observation = 1.0f;
            universe.observationValue = 2.0f;
            universe.capacityOfTerms = 10;
            universe.sizeOfTerms = 2;
            universe.termList = termList;

//when
        const char *string = FRI_universeToString(&universe);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string,
                "\"universe\", capacityOfTerms: 10, sizeOfTerms: 2, observation: 1.000000, observationValue: 2.000000\n"
                "\t0. \"term1\", center: 12.000000, value: 1.000000\n"
                "\t1. \"term2\", center: 12.000000, value: 1.000000");
        free(string);
)

CESTER_TEST(FRI_universeToString__NAME_NULL, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
                    struct FRI_UNIVERSE universe;
                    universe.name = NULL;
                    universe.observation = 1.0f;
                    universe.observationValue = 2.0f;
                    universe.capacityOfTerms = 10;
                    universe.sizeOfTerms = 2;
                    universe.termList = termList;

//when
        const char *string = FRI_universeToString(&universe);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string,
                                "\"\", capacityOfTerms: 10, sizeOfTerms: 2, observation: 1.000000, observationValue: 2.000000\n"
                                "\t0. \"term1\", center: 12.000000, value: 1.000000\n"
                                "\t1. \"term2\", center: 12.000000, value: 1.000000");
        free(string);
)

CESTER_TEST(FRI_antecedentToString__NULL, inst,
// given

//when
            const char *string = FRI_antecedentToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_antecedentToString__NOT_NULL, inst,
// given
        struct ANTECEDENT antecedent;
        struct FRI_TERM termList[2];
        termList[0].center = 12.0f;
        termList[0].value = 1.0f;
        termList[0].name = "term1";
        termList[1].center = 12.0f;
        termList[1].value = 1.0f;
        termList[1].name = "term2";
        struct FRI_UNIVERSE universe;
        universe.name = "universe";
        universe.observation = 1.0f;
        universe.observationValue = 2.0f;
        universe.capacityOfTerms = 10;
        universe.sizeOfTerms = 2;
        universe.termList = termList;

        antecedent.predicate = &universe;
        antecedent.termIdForAntecedent = 1;
        antecedent.distance = 23;
//when
        const char *string = FRI_antecedentToString(&antecedent);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string,"predicate: \"universe\", termIdForAntecedent: 1, distance: 23.000000");
)

CESTER_TEST(FRI_ruleToString__NULL, inst,
// given

//when
            const char *string = FRI_ruleToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_ruleToString__NOT_NULL, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
                    struct FRI_UNIVERSE universeList[1];
                    universeList[0].name = "first antecedent";
                    universeList[0].observation = 1.0f;
                    universeList[0].observationValue = 2.0f;
                    universeList[0].capacityOfTerms = 10;
                    universeList[0].sizeOfTerms = 2;
                    universeList[0].termList = termList;
                    struct ANTECEDENT antecedentList[1];
                    antecedentList[0].predicate = &universeList[0];
                    antecedentList[0].distance = 2.3f;
                    antecedentList[0].termIdForAntecedent = 0;
                    struct FRI_RULE rule;
                    rule.termIdForConsequent = 1;
                    rule.numberOfAntecedents = 1;
                    rule.capacityOfAntecedents = 3;
                    rule.antecedents = antecedentList;
                    rule.distance = 5.6f;
                    rule.description = "description";
//when
            const char *string = FRI_ruleToString(&rule);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "description: \"description\", distance: 5.600000, capacityOfAntecedents: 3, numberOfAntecedents: 1, termIdForConsequent: 1\n"
                                        "\t0. antecedent predicate: \"first antecedent\", termIdForAntecedent: 0, distance: 2.300000");
)

CESTER_TEST(FRI_ruleToString__NULL_DESCRIPTION, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
                    struct FRI_UNIVERSE universeList[1];
                    universeList[0].name = "first antecedent";
                    universeList[0].observation = 1.0f;
                    universeList[0].observationValue = 2.0f;
                    universeList[0].capacityOfTerms = 10;
                    universeList[0].sizeOfTerms = 2;
                    universeList[0].termList = termList;
                    struct ANTECEDENT antecedentList[1];
                    antecedentList[0].predicate = &universeList[0];
                    antecedentList[0].distance = 2.3f;
                    antecedentList[0].termIdForAntecedent = 0;
                    struct FRI_RULE rule;
                    rule.termIdForConsequent = 1;
                    rule.numberOfAntecedents = 1;
                    rule.capacityOfAntecedents = 3;
                    rule.antecedents = antecedentList;
                    rule.distance = 5.6f;
                    rule.description = NULL;
//when
        const char *string = FRI_ruleToString(&rule);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "description: \"-\", distance: 5.600000, capacityOfAntecedents: 3, numberOfAntecedents: 1, termIdForConsequent: 1\n"
                                        "\t0. antecedent predicate: \"first antecedent\", termIdForAntecedent: 0, distance: 2.300000");
)

CESTER_TEST(FRI_ruleToString__NULL_ANTECEDENT_NAME, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
                    struct FRI_UNIVERSE universeList[1];
                    universeList[0].name = NULL;
                    universeList[0].observation = 1.0f;
                    universeList[0].observationValue = 2.0f;
                    universeList[0].capacityOfTerms = 10;
                    universeList[0].sizeOfTerms = 2;
                    universeList[0].termList = termList;
                    struct ANTECEDENT antecedentList[1];
                    antecedentList[0].predicate = &universeList[0];
                    antecedentList[0].distance = 2.3f;
                    antecedentList[0].termIdForAntecedent = 0;
                    struct FRI_RULE rule;
                    rule.termIdForConsequent = 1;
                    rule.numberOfAntecedents = 1;
                    rule.capacityOfAntecedents = 3;
                    rule.antecedents = antecedentList;
                    rule.distance = 5.6f;
                    rule.description = "description";
//when
        const char *string = FRI_ruleToString(&rule);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "description: \"description\", distance: 5.600000, capacityOfAntecedents: 3, numberOfAntecedents: 1, termIdForConsequent: 1\n"
                                        "\t0. antecedent predicate: \"-\", termIdForAntecedent: 0, distance: 2.300000");
)

CESTER_TEST(FRI_ruleBaseToString__NULL, inst,
// given

//when
            const char *string = FRI_ruleBaseToString(NULL);
//then
        cester_assert_null(string);
)

CESTER_TEST(FRI_ruleBaseToString__NOT_NULL, inst,
// given
            struct FRI_TERM termList[2];
                    termList[0].center = 12.0f;
                    termList[0].value = 1.0f;
                    termList[0].name = "term1";
                    termList[1].center = 12.0f;
                    termList[1].value = 1.0f;
                    termList[1].name = "term2";
                    struct FRI_UNIVERSE universeList[1];
                    universeList[0].name = "first antecedent";
                    universeList[0].observation = 1.0f;
                    universeList[0].observationValue = 2.0f;
                    universeList[0].capacityOfTerms = 10;
                    universeList[0].sizeOfTerms = 2;
                    universeList[0].termList = termList;
                    struct ANTECEDENT antecedentList[1];
                    antecedentList[0].predicate = &universeList[0];
                    antecedentList[0].distance = 2.3f;
                    antecedentList[0].termIdForAntecedent = 0;
                    struct FRI_RULE ruleList[1];
                    ruleList[0].termIdForConsequent = 1;
                    ruleList[0].numberOfAntecedents = 1;
                    ruleList[0].capacityOfAntecedents = 3;
                    ruleList[0].antecedents = antecedentList;
                    ruleList[0].distance = 5.6f;
                    ruleList[0].description = "description";

                    struct FRI_RULEBASE ruleBase;
                    ruleBase.capacityOfRules = 5;
                    ruleBase.numberOfRules = 1;
                    ruleBase.rules = ruleList;

//when
            const char *string = FRI_ruleBaseToString(&ruleBase);
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, "capacityOfRules: 5, numberOfRules: 1\n"
                                        "0. description: \"description\", distance: 5.600000, capacityOfAntecedents: 3, numberOfAntecedents: 1, termIdForConsequent: 1\n"
                                        "\t0. antecedent predicate: \"first antecedent\", termIdForAntecedent: 0, distance: 2.300000");
)


CESTER_TEST(FRI_toString__NOT_NULL, inst,
// given
            static struct FRI_UNIVERSE universeList[2];
                    static struct FRI_TERM distanceTermList[2];
                    struct FRI_UNIVERSE *pDistanceUniverse = &universeList[0];
                    struct FRI_UNIVERSE *pSpeedUniverse = &universeList[1];
                    struct FRI_TERM *pCloseDistance = &distanceTermList[0];
                    struct FRI_TERM *pFarDistance = &distanceTermList[1];
                    pCloseDistance->center = 0;
                    pCloseDistance->value = 0;
                    pCloseDistance->name = "close";
                    pFarDistance->center = 720;
                    pFarDistance->value = 1;
                    pFarDistance->name = "far";

                    pDistanceUniverse->termList = distanceTermList;
                    pDistanceUniverse->name = "distance";
                    pDistanceUniverse->capacityOfTerms = 2;
                    pDistanceUniverse->sizeOfTerms = 2;

                    static struct FRI_TERM speedUniverseTermList[2];
                    static struct FRI_TERM *pLowSpeedTerm;
                    pLowSpeedTerm = &speedUniverseTermList[0];
                    pLowSpeedTerm->center = 0;
                    pLowSpeedTerm->value = 0;
                    pLowSpeedTerm->name = "low";
                    static struct FRI_TERM *pHighSpeedTerm;
                    pHighSpeedTerm = &speedUniverseTermList[1];
                    pHighSpeedTerm->center = 1;
                    pHighSpeedTerm->value = 1;
                    pHighSpeedTerm->name = "high";

                    pSpeedUniverse->termList = speedUniverseTermList;
                    pSpeedUniverse->name = "speed";
                    pSpeedUniverse->capacityOfTerms = 2;
                    pSpeedUniverse->sizeOfTerms = 2;

                    static struct FRI_RULE ruleList[2];
                    struct FRI_RULE *pCloseDistanceLowSpeedRule;
                    pCloseDistanceLowSpeedRule = &ruleList[0];
                    pCloseDistanceLowSpeedRule->capacityOfAntecedents = 1;
                    pCloseDistanceLowSpeedRule->numberOfAntecedents = 1;
                    pCloseDistanceLowSpeedRule->termIdForConsequent = 0;
                    static struct ANTECEDENT antecedent1;
                    antecedent1.predicate = pDistanceUniverse;
                    antecedent1.termIdForAntecedent = 0;
                    pCloseDistanceLowSpeedRule->antecedents = &antecedent1;
                    pCloseDistanceLowSpeedRule->description = "IF distance IS close THEN speed IS low";

                    struct FRI_RULE *pFarDistanceHighSpeedRule;
                    pFarDistanceHighSpeedRule = &ruleList[1];
                    pFarDistanceHighSpeedRule->capacityOfAntecedents = 1;
                    pFarDistanceHighSpeedRule->numberOfAntecedents = 1;
                    pFarDistanceHighSpeedRule->termIdForConsequent = 1;
                    static struct ANTECEDENT antecedent2;
                    antecedent2.predicate = pDistanceUniverse;
                    antecedent2.termIdForAntecedent = 1;
                    pFarDistanceHighSpeedRule->antecedents = &antecedent2;
                    pFarDistanceHighSpeedRule->description = "IF distance IS far THEN speed IS high";

                    static struct FRI_RULEBASE ruleBaseList[1];
                    struct FRI_RULEBASE *pSpeedRuleBase;
                    pSpeedRuleBase = &ruleBaseList[0];
                    pSpeedRuleBase->capacityOfRules = 2;
                    pSpeedRuleBase->numberOfRules = 2;
                    pSpeedRuleBase->consequent = pSpeedUniverse;
                    pSpeedRuleBase->rules = ruleList;

                    g_universes = universeList;
                    g_ruleBases = ruleBaseList;
                    g_numberOfUniverses = 2;
                    g_numberOfRuleBases = 1;
                    g_lastInitializedRuleBase = 0;
                    g_lastInitializedUniverse = 1;
                    g_lastInitializedRule = 2;

                    const char *expectedResult = "\nUniverses\n"
                                                 "0. \"distance\", capacityOfTerms: 2, sizeOfTerms: 2, observation: 0.000000, observationValue: 0.000000\n"
                                                 "\t0. \"close\", center: 0.000000, value: 0.000000\n"
                                                 "\t1. \"far\", center: 720.000000, value: 1.000000\n"
                                                 "1. \"speed\", capacityOfTerms: 2, sizeOfTerms: 2, observation: 0.000000, observationValue: 0.000000\n"
                                                 "\t0. \"low\", center: 0.000000, value: 0.000000\n"
                                                 "\t1. \"high\", center: 1.000000, value: 1.000000\n"
                                                 "\n"
                                                 "Rule bases\n"
                                                 "\n"
                                                 "capacityOfRules: 2, numberOfRules: 2\n"
                                                 "0. description: \"IF distance IS close THEN speed IS low\", distance: 0.000000, capacityOfAntecedents: 1, numberOfAntecedents: 1, termIdForConsequent: 0\n"
                                                 "\t0. antecedent predicate: \"distance\", termIdForAntecedent: 0, distance: 0.000000\n"
                                                 "1. description: \"IF distance IS far THEN speed IS high\", distance: 0.000000, capacityOfAntecedents: 1, numberOfAntecedents: 1, termIdForConsequent: 1\n"
                                                 "\t0. antecedent predicate: \"distance\", termIdForAntecedent: 1, distance: 0.000000";

//when
            const char *string = FRI_toString();
//then
        cester_assert_not_null(string);
        cester_assert_str_equal(string, expectedResult);
)

CESTER_BODY(
        int main(int argc, char** argv) {

            CESTER_REGISTER_TEST(FRI_termToString__NULL);
            CESTER_REGISTER_TEST(FRI_termToString__NOT_NULL);
            CESTER_REGISTER_TEST(FRI_termToString__NAME_NULL);

            CESTER_REGISTER_TEST(FRI_universeToString__NULL);
            CESTER_REGISTER_TEST(FRI_universeToString__NOT_NULL);
            CESTER_REGISTER_TEST(FRI_universeToString__NAME_NULL);

            CESTER_REGISTER_TEST(FRI_antecedentToString__NULL);
            CESTER_REGISTER_TEST(FRI_antecedentToString__NOT_NULL);

            CESTER_REGISTER_TEST(FRI_ruleToString__NULL);
            CESTER_REGISTER_TEST(FRI_ruleToString__NOT_NULL);
            CESTER_REGISTER_TEST(FRI_ruleToString__NULL_DESCRIPTION);
            CESTER_REGISTER_TEST(FRI_ruleToString__NULL_ANTECEDENT_NAME);

            CESTER_REGISTER_TEST(FRI_ruleBaseToString__NULL);
            CESTER_REGISTER_TEST(FRI_ruleBaseToString__NOT_NULL);

            CESTER_REGISTER_TEST(FRI_toString__NOT_NULL);

            return CESTER_RUN_ALL_TESTS(argc, argv);
        }
)
