//
// Created by Kry on 11/4/2020.
//

#include <ufive/uFRI.h>
#include <ufive/uError.h>
#include <assert.h>
#include <stdio.h>
int FRI_calculateRuleDistanceFromObservation(struct FRI_RULE *pRule);

int FRI_initZ() {
    g_numberOfUniverses = 0;
    g_numberOfRuleBases = 0;
    return OK;
}

int FRI_init(uint16_t p_numberOfUniverses, uint16_t p_numberOfRuleBases) {
    assert(p_numberOfUniverses > 0 && "number of g_universes must be greater than zero");
    assert(p_numberOfRuleBases > 0 && "rule base measurement be greater than zero");
    int rc = FRI_createUniverseArray(p_numberOfUniverses, &g_universes);
    if (rc != OK) {
        return rc;
    }
    return FRI_initRuleBasesArray(p_numberOfRuleBases, &g_ruleBases);
}

int FRI_createUniverseArray(const uint16_t p_numberOfUniverses, struct FRI_UNIVERSE **pUniverses) {
    if (p_numberOfUniverses == 0) {
        return WRONG_PARAMETER;
    }

    g_numberOfUniverses = p_numberOfUniverses;
    g_lastInitializedUniverse = 0;
    *pUniverses = calloc(g_numberOfUniverses, sizeof(struct FRI_UNIVERSE));

    if (*pUniverses == NULL) {
        return UNIVERSE_FAILED_TO_GENERATE;
    }

    return OK;
}

int FRI_initUniverseByPtr(struct FRI_UNIVERSE *universe, uint16_t numberOfUniverseData, const char const *name) {
    if (universe == NULL || numberOfUniverseData == 0) {
        return WRONG_PARAMETER;
    }

    struct FRI_TERM *pData = calloc(numberOfUniverseData, sizeof(struct FRI_TERM));

    if (pData == NULL) {
        return UNIVERSE_FAILED_TO_GENERATE;
    }

    universe->capacityOfTerms = numberOfUniverseData;
    universe->sizeOfTerms = 0;
    universe->termList = pData;
    universe->name = name;
    return OK;
}

int FRI_initUniverseById(uint16_t id, uint16_t numberOfUniverseData, const char const *name) {
    g_lastInitializedUniverse = id;
    return  FRI_initUniverseByPtr(&g_universes[id], numberOfUniverseData, name);
}

int FRI_addUniverseDataByPtr(struct FRI_UNIVERSE *pDestination, float center, float value, char *name) {
    if (pDestination->sizeOfTerms < pDestination->capacityOfTerms) {
        pDestination->termList[pDestination->sizeOfTerms].center = center;
        pDestination->termList[pDestination->sizeOfTerms].value = value;
        pDestination->termList[pDestination->sizeOfTerms].name = name;
        pDestination->sizeOfTerms++;
        return pDestination->sizeOfTerms;
    }
    return -1;
}

int FRI_addUniverseDataById(uint16_t id, float center, float value, const char const *name) {
    return FRI_addUniverseDataByPtr(&g_universes[id], center, value, name);
}

int FRI_initRuleBasesArray(uint16_t p_numberOfRuleBases, struct FRI_RULEBASE **p_ruleBases) {

    if (p_numberOfRuleBases == 0) {
        return WRONG_PARAMETER;
    }
    *p_ruleBases = calloc(p_numberOfRuleBases, sizeof(struct FRI_RULEBASE));
    if (*p_ruleBases == NULL) {
        return RULE_BASES_FAILED_TO_GENERATE;
    }

    g_numberOfRuleBases = p_numberOfRuleBases;
    g_lastInitializedRule = 0;
    g_lastInitializedRuleBase = 0;

    return OK;
}

int FRI_initRuleBaseByPtr(struct FRI_RULEBASE *pRuleBase, uint16_t capacityOfRules, struct FRI_UNIVERSE *pConsequentUniverse) {
    if (pRuleBase == NULL || pConsequentUniverse == NULL) {
        return WRONG_PARAMETER;
    }
    pRuleBase->capacityOfRules = capacityOfRules;
    pRuleBase->consequent = pConsequentUniverse;
    pRuleBase->rules = calloc(capacityOfRules, sizeof(struct FRI_RULE));
    return OK;
}

void FRI_initRuleBaseById(uint16_t idOfRuleBase, uint16_t numberOfRules, uint16_t idOfUniverse) {
    //  TODO: check index < sizeOfTerms and > 0
    FRI_initRuleBaseByPtr(&g_ruleBases[idOfRuleBase], numberOfRules, &g_universes[idOfUniverse]);
    g_lastInitializedRuleBase = idOfRuleBase;
    g_lastInitializedRule = 0;
}

int FRI_initRuleByPtr(struct FRI_RULE *pRule, uint16_t capacityOfAntecedents, uint16_t consequentData) {
    if (pRule == NULL) {
        return WRONG_PARAMETER;
    }
    pRule->capacityOfAntecedents=capacityOfAntecedents;
    pRule->termIdForConsequent = consequentData;
    pRule->antecedents = calloc((capacityOfAntecedents), sizeof(struct ANTECEDENT));
    return OK;
}

void FRI_initRuleById(uint16_t idOfRuleBase, uint16_t idOfRule, uint16_t numberOfAntecedents, uint16_t consequentData) {
    //  TODO: check index < sizeOfTerms

    if (g_ruleBases[idOfRuleBase].numberOfRules < g_ruleBases[idOfRuleBase].capacityOfRules) {
        FRI_initRuleByPtr(&g_ruleBases[idOfRuleBase].rules[idOfRule], numberOfAntecedents, consequentData);
        g_ruleBases[idOfRuleBase].numberOfRules++;
        g_lastInitializedRule = idOfRule;
    }
}

int FRI_addAntecedentToRuleByPtr(struct FRI_RULE *pRule, struct FRI_UNIVERSE *pPredicate, uint16_t p_universeDataForAntecedent) {
    int state = -1;

    if(pRule->numberOfAntecedents < pRule->capacityOfAntecedents)
    {
        pRule->antecedents[pRule->numberOfAntecedents].predicate = pPredicate;
        pRule->antecedents[pRule->numberOfAntecedents].termIdForAntecedent = p_universeDataForAntecedent;
        state = pRule->numberOfAntecedents++;
    }
    return state;
}

int FRI_addAntecedentToRuleById(uint16_t idOfRuleBase, uint16_t idOfRule,
                                uint16_t idOfAntecedentUniverse, uint16_t p_universeDataForAntecedent) {
    return FRI_addAntecedentToRuleByPtr(&g_ruleBases[idOfRuleBase].rules[idOfRule],
                                        &g_universes[idOfAntecedentUniverse], p_universeDataForAntecedent);
}

int FRI_addUniverseElement(float x, float y, const char const *name) {
    return FRI_addUniverseDataById(g_lastInitializedUniverse, x, y, name);
}

void FRI_addRuleToRuleBase(uint16_t consequentUniverseData, uint16_t numberOfAntecedents) {
    FRI_initRuleById(g_lastInitializedRuleBase, g_lastInitializedRule, numberOfAntecedents, consequentUniverseData);
    g_lastInitializedRule++;
}

void FRI_addAntecedentToRule(uint16_t antecedentUniverse, uint16_t elementOfUniverseForAntecedent) {
    FRI_addAntecedentToRuleById(g_lastInitializedRuleBase, g_lastInitializedRule - 1, antecedentUniverse, elementOfUniverseForAntecedent);
}

void FRI_clean() {
    int i,j;
    for (i = 0 ; i < g_numberOfUniverses ; i++){
        free(g_universes[i].termList);
    }

    for (i = 0 ; i < g_numberOfRuleBases; i++){
        for (j=0 ; j < g_ruleBases[i].numberOfRules ; j++){
            free(g_ruleBases[i].rules[j].antecedents);
        }
        free(g_ruleBases[i].rules);
    }

    free(g_universes);
    free(g_ruleBases);
    g_numberOfRuleBases = 0;
    g_numberOfUniverses = 0;
}


struct FRI_UNIVERSE *FRI_getUniverse(uint16_t idOfUniverse) {
    return &g_universes[idOfUniverse];
}

float FRI_getObservationById(uint16_t idOfUniverse) {
    if (idOfUniverse < g_numberOfUniverses) {
        return g_universes[idOfUniverse].observation;
    } else {
        return INT16_MIN;
    }
}

void FRI_calculateRuleBaseById(uint16_t idOfRuleBase) {
    uint16_t i;
    for (i = 0; i < g_ruleBases[idOfRuleBase].numberOfRules; ++i) {
        FRI_calculateRuleDistanceFromObservation(&g_ruleBases[idOfRuleBase].rules[i]);
    }
    FRI_shepard(&g_ruleBases[idOfRuleBase]);
}

void FRI_setObservationForUniverseById(uint16_t id, float observation) {
    if (id < g_numberOfUniverses) {
        FRI_setObservationForUniverse(&g_universes[id], observation);
    }
}