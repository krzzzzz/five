//
// Created by Kry on 11/4/2020.
//

#define CESTER_NO_MAIN
#include <exotic/cester.h>
#include <ufive/uFRI.h>
#include <ufive/uError.h>

#ifndef CALLOC_MOCK
#define CALLOC_MOCK
int * calloc_rv;
int FRI_calculateRuleDistanceFromObservation(struct FRI_RULE *pRule);

struct CALLOC_PARAMS {
    size_t nmemb;
    size_t size;
} callocParams;
#endif
CESTER_MOCK_FUNCTION(calloc(size_t nmemb, size_t size), void *,
                     callocParams.nmemb = nmemb;
                             callocParams.size = size;
                             return calloc_rv;
)

CESTER_TEST(FRI_addUniverseDataById__NULL, inst,
    float result = 0.0;
    int errorCode = FRI_linear_interpolation(NULL, &result);
    cester_assert_equal(errorCode, WRONG_PARAMETER);
)

CESTER_BODY(
        int main(int argc, char** argv) {
    CESTER_REGISTER_TEST(FRI_addUniverseDataById__NULL);

    return CESTER_RUN_ALL_TESTS(argc, argv);
}
)
