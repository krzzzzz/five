//
// Created by Kry on 11/5/2020.
//

#include <string.h>
#include <stdio.h>

#include <ufive/uFRI.h>


char * cloneString(const char const *source) {
    char *result = (char *) calloc(strlen(source) + 1, sizeof(char));
    strcpy(result, source);
    return result;
}

char *FRI_termToString(const struct FRI_TERM const *pTerm) {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    rTemp[0] = '\0';

    if (pTerm == NULL) { return NULL;}

    sprintf(rTemp, "\"%s\", center: %f, value: %f",
            pTerm->name == NULL ? "" : pTerm->name, pTerm->center, pTerm->value);
    return cloneString(rTemp);
}

char *FRI_universeToString(const struct FRI_UNIVERSE const *pUniverse) {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    char temp[1000];
    rTemp[0] = '\0';

    if (pUniverse == NULL) { return NULL;}

    sprintf(temp, "\"%s\", capacityOfTerms: %d, sizeOfTerms: %d, observation: %f, observationValue: %f",
            pUniverse->name == NULL ? "" : pUniverse->name, pUniverse->capacityOfTerms, pUniverse->sizeOfTerms, pUniverse->observation, pUniverse->observationValue);
    strcat(rTemp, temp);

    struct FRI_TERM *pTerm;
    for (int j = 0 ; j < pUniverse->sizeOfTerms ; j++) {
        pTerm = &pUniverse->termList[j];
        const char* termString = FRI_termToString(pTerm);
        sprintf(temp, "\n\t%d. ", j);
        strcat(rTemp, temp);
        strcat(rTemp, termString);
        free(termString);
    }

    return cloneString(rTemp);
}

char *FRI_antecedentToString(const struct ANTECEDENT const *pAntecedent) {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    char temp[1000];
    rTemp[0] = '\0';

    if (pAntecedent == NULL) { return NULL;}

    if (pAntecedent->predicate != NULL) {
        sprintf(temp, "predicate: \"%s\", ", pAntecedent->predicate->name == NULL ? "-" :  pAntecedent->predicate->name);
        strcat(rTemp, temp);
    }

    sprintf(temp, "termIdForAntecedent: %d, distance: %f", pAntecedent->termIdForAntecedent, pAntecedent->distance );
    strcat(rTemp, temp);
    return cloneString(rTemp);
}

char *FRI_ruleToString(const struct FRI_RULE *pRule) {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    char temp[1000];
    rTemp[0] = '\0';

    if (pRule == NULL) { return NULL;}

    sprintf(rTemp, "description: \"%s\", distance: %f, capacityOfAntecedents: %d, numberOfAntecedents: %d, termIdForConsequent: %d",
            pRule->description ? pRule->description : "-", pRule->distance, pRule->capacityOfAntecedents, pRule->numberOfAntecedents, pRule->termIdForConsequent);

    for (int i = 0; i < pRule->numberOfAntecedents; i++) {
        const char* antecedentString = FRI_antecedentToString(&pRule->antecedents[i]);
        sprintf(temp, "\n\t%d. antecedent ", i);
        strcat(rTemp, temp);
        strcat(rTemp, antecedentString);
        free(antecedentString);
    }

    return cloneString(rTemp);
}

char *FRI_ruleBaseToString(const struct FRI_RULEBASE *pRuleBase) {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    char temp[1000];
    rTemp[0] = '\0';

    if (pRuleBase == NULL) { return NULL;}

    sprintf(temp, "capacityOfRules: %d, numberOfRules: %d",
            pRuleBase->capacityOfRules, pRuleBase->numberOfRules);
    strcat(rTemp, temp);
    struct FRI_RULE *pRule;
    for (int j = 0 ; j < pRuleBase->numberOfRules ; j++) {
        pRule = &pRuleBase->rules[j];
        const char *ruleString = FRI_ruleToString(pRule);
        sprintf(temp, "\n%d. ", j);
        strcat(rTemp, temp);
        strcat(rTemp, ruleString);
        free(ruleString);
    }

    return cloneString(rTemp);
}

char *FRI_toString() {
    char rTemp[MAX_TEMP_STRING_LENGTH];
    rTemp[0] = '\0';

    strcat(rTemp, "\nUniverses");
    char temp[1000];
    strcpy(temp, "");
    struct FRI_UNIVERSE *pUniverse;

    for (int i = 0 ; i < g_numberOfUniverses ; i++) {
        pUniverse = &g_universes[i];
        const char* universeString = FRI_universeToString(pUniverse);
        sprintf(temp, "\n%d. ", i);
        strcat(rTemp, temp);
        strcat(rTemp, universeString);
        free(universeString);
    }

    strcat(rTemp, "\n\nRule bases");

    struct FRI_RULEBASE *pRuleBase;
    strcpy(temp, "");
    for (int i = 0 ; i < g_numberOfRuleBases ; i++) {
        pRuleBase = &g_ruleBases[i];
        const char *ruleBaseString = FRI_ruleBaseToString(pRuleBase);
        strcat(rTemp, "\n\n");
        strcat(rTemp, ruleBaseString);
    }
    return cloneString(rTemp);
}